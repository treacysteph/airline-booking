import logo from './logo.svg';
import './App.css';
import Counter from './Counter';
import Emlpoyee from './Employee';

function App() {
  const employeeInfo = [
    {
      firstName: "Jack",
      lastName: "A",
      age: "13"
    },
    {
      firstName: "Stephanie",
      lastName: "B",
      age: "13"
    },
    {
      firstName: "George",
      lastName: "C",
      age: "13"
    },
  ];
  return (
    <div className="App">
      <header className="App-header">
        {employeeInfo.map((employee) => {
          const { firstName, lastName, age } = employee;
          return (
            <h2>{`name: ${firstName} ${lastName} age: ${age}`}</h2>
          );
        })}
      </header>
    </div>
  );
}

export default App;
