import React from 'react';

const Employee = (props) => {
  const { firstName, lastName } = props;
  return (
    <React.Fragment>
      <h3>{`Employee Name: ${firstName} ${lastName}`}</h3>

    </React.Fragment>
  );

}

export default Employee;